package com.nttdata.clientserv.client.domain.exception;

import com.nttdata.clientserv.client.domain.ClientID;
import com.nttdata.shared.domain.DomainError;

public class ClientNotFoundException extends DomainError {

    public ClientNotFoundException(ClientID id) {
        super("client_not_exist", String.format("The client with doctype %s and docNumber doesn't exist",
                id.docTypeValue(), id.documentNumberValue()));
    }

}
