package com.nttdata.clientserv.client.domain;

import lombok.Data;

@Data
public class ClientID {
    private DocumentType documentType;
    private String documentNumber;

    public ClientID(String documentType, String documentNumeber) {
        // todo: validate docType
        // todo: validate docNumber
        this.documentType = DocumentType.valueOf(documentType);
        this.documentNumber = documentNumeber;
    }

    public String documentNumberValue() {
        return this.documentNumber;
    }

    public String docTypeValue() {
        return this.documentType.name();
    }
}
