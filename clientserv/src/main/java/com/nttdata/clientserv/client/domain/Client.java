package com.nttdata.clientserv.client.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Client {
    private ClientID id;
    private String firstName;
    private String middleName;
    private String firstLastName;
    private String secondLastName;
    private Phone phone;
    private String address;
    private City city;
}
