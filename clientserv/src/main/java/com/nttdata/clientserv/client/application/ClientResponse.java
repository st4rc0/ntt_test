package com.nttdata.clientserv.client.application;

import com.nttdata.clientserv.client.domain.Client;

public record ClientResponse(String firstName,
                String middleName,
                String firstLastName,
                String secondLastName,
                String phone,
                String address,
                String city) {

        public static ClientResponse fromAggregate(Client client) {
                return new ClientResponse(client.getFirstName(),
                                client.getMiddleName(),
                                client.getFirstLastName(),
                                client.getSecondLastName(),
                                client.getPhone().value(),
                                client.getAddress(),
                                client.getCity().value());
        }
}
