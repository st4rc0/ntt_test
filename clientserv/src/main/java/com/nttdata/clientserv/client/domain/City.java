package com.nttdata.clientserv.client.domain;

public class City {
    private String city;

    public City(String city) {
        // todo: validate city;
        this.city = city;
    }

    public String value() {
        return this.city;
    }
}
