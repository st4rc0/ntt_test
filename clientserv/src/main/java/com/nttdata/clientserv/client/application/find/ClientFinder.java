package com.nttdata.clientserv.client.application.find;

import org.springframework.stereotype.Service;

import com.nttdata.clientserv.client.application.ClientResponse;
import com.nttdata.clientserv.client.domain.Client;
import com.nttdata.clientserv.client.domain.ClientID;
import com.nttdata.clientserv.client.domain.exception.ClientNotFoundException;
import com.nttdata.clientserv.client.infrastructure.ClientRepository;

@Service
public record ClientFinder(ClientRepository clientRepo) {
    public ClientResponse find(String documentType, String documentNumber) throws ClientNotFoundException {

        ClientID id = new ClientID(documentType, documentNumber);

        Client client = clientRepo.findByID(id).orElseThrow(() -> new ClientNotFoundException(id));

        return ClientResponse.fromAggregate(client);
    }
}
