package com.nttdata.clientserv.client.application;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.nttdata.clientserv.client.application.find.ClientFinder;
import com.nttdata.clientserv.client.domain.exception.ClientNotFoundException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v1/clients")
public record ClientController(ClientFinder clientFinder) {

    @GetMapping("id")
    public ClientResponse getClient(@RequestParam(required = false) String docType,
            @RequestParam(required = true) String docNumber) {

        log.info("Client request docType: {} docNumber: {}", docType, docNumber);

        try {
            return clientFinder.find(docType, docNumber);
        } catch (ClientNotFoundException exc) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, exc.errorMessage());
        }
    }
}
