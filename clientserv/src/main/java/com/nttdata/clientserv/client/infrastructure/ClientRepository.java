package com.nttdata.clientserv.client.infrastructure;

import java.util.Optional;

import com.nttdata.clientserv.client.domain.Client;
import com.nttdata.clientserv.client.domain.ClientID;

public interface ClientRepository {

    public Optional<Client> findByID(ClientID clientID);

    public void saveClient(Client client);
}
