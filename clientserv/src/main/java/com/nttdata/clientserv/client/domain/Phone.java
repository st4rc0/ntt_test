package com.nttdata.clientserv.client.domain;

public class Phone {
    private String code;
    private String number;

    public Phone(String code, String number) {
        // todo: validate code
        // todo: validate number

        this.code = code;
        this.number = number;
    }

    public String value() {
        return String.format("%s %s", code, number);
    }
}
