package com.nttdata.clientserv.client.infrastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.nttdata.clientserv.client.domain.City;
import com.nttdata.clientserv.client.domain.Client;
import com.nttdata.clientserv.client.domain.ClientID;
import com.nttdata.clientserv.client.domain.Phone;

@Repository
public class InMemoryClientRepository implements ClientRepository {
    private static final Map<ClientID, Client> CLIENT_BY_ID;
    static {
        CLIENT_BY_ID = new HashMap<>();
        ClientID clientID = new ClientID("C", "23445322");
        CLIENT_BY_ID.put(clientID, Client.builder()
                .id(clientID)
                .firstName("Cesar")
                .middleName("Mauricio")
                .firstLastName("Gonzalez")
                .secondLastName("Ramos")
                .phone(new Phone("+57","3041234567"))
                .address("Calle 123 # 45 - 67")
                .city(new City("Cali"))
                .build());
    }

    @Override
    public void saveClient(Client client) {
        CLIENT_BY_ID.put(client.getId(), client);

    }

    @Override
    public Optional<Client> findByID(ClientID clientID) {
        return Optional.ofNullable(CLIENT_BY_ID.get(clientID));
    }

}
